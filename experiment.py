#!/usr/bin/env python3

import glob
import os
import subprocess
import sys

import click

# Tools definition
TOOLS_PATH = os.path.join("./tools", sys.platform)
GENERATOR_TOOL = os.path.join(TOOLS_PATH, "generator")
MODGRAPH_TRANSLATOR = os.path.join(TOOLS_PATH, "adt2amas")
TREE_REDUCE_TOOL = os.path.join(TOOLS_PATH, "tree_reduce")
SYMPROD_TOOL = os.path.join(TOOLS_PATH, "symprod")
STATE_SPACE_CHECKER = os.path.join(TOOLS_PATH, "checker.py")


def run_command(command, timeout=None, verbose=False, input=None):
    """
    Wrapper to run a ``command`` using python
    """
    if verbose:
        print(f"((timeout: {timeout} sec))")
        print("calling " + command)

    try:
        print(
            subprocess.run(
                command.split(" "),
                input=input,
                shell=False,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                encoding="ascii",
                timeout=timeout,
            ).stdout
        )
    except subprocess.TimeoutExpired:
        print("<<< timeout >>>")


def generate_command(width, depth, children, filename, tool=GENERATOR_TOOL):
    """ "
    Run the ``tool`` to generate an ADTree with a specific ``width``,
    ``depth``, and number of ``children``. The output will be saved in the
    ``filename`` file.
    """
    command = f"{tool} -w {width} -d {depth} -c {children} -f {filename}"
    return run_command(command)


def transformer_command(filename, option="--full-model", tool=MODGRAPH_TRANSLATOR):
    """
    Run the ``tool`` to translate an ADTree defined in the ``filename`` file
    into an EAMAS system. The output is saved using the modgraph syntax.
    """
    command = f"{tool} transform --model {filename} --output-format modgraph {option}"
    return run_command(command)


def get_modgraph_files(
    model_dir, sync_filename="sync.modgraph", root_marker="-top.modgraph"
):
    """
    Returns the modgraph files in the ``model_dir`` directory.
    It will return a pair like: ('model_dir/sync.modgraph', ['xxx1-top.modgraph', 'xxx2.modgraph', ...])
    """
    modgraph_files = []
    for f in glob.glob(f"{model_dir}/*.modgraph"):
        if not f.endswith(sync_filename):
            if f.endswith(root_marker):
                modgraph_files.insert(0, f)
            else:
                modgraph_files.append(f)

    return (f"{model_dir}/{sync_filename}", modgraph_files)


def run_tree_reduce(
    modgraph_files, tool=TREE_REDUCE_TOOL, tree_type="sync-deadlock", timeout=1800
):
    """
    Run ``tool`` to return the sum-of-squares reduction of the network.
    """
    topology_type_switch = "-o" if tree_type == "sync-deadlock" else ""

    protocol = modgraph_files[0]
    modules = " ".join(modgraph_files[1])
    command = f"{tool} -m {modules} -s {protocol} {topology_type_switch}"

    return run_command(command, timeout, True)


def get_modgraph_folder(f, suffix=""):
    """Move the generated modgraph model to the correspoding case study folder"""
    folder = os.path.dirname(f)
    model = os.path.splitext(os.path.basename(f))[0]

    # save .tex file
    os.system(f"mv {model}.tex {model}/")

    # replace old files
    dest_folder = os.path.join(folder, f"{model}{suffix}")
    os.system(f"rm -rf {dest_folder}")
    os.system(f"mv {model} {dest_folder}")

    return dest_folder


def run_prod(modgraph_files, tool=SYMPROD_TOOL, timeout=1800, tool_input="1\n9\n"):
    """Run the symprod tool"""
    protocol = modgraph_files[0]
    modules = " ".join(modgraph_files[1])
    command = f"{tool} {protocol} {modules}"

    return run_command(command, timeout, True, tool_input)


def run_single_experiment(
    fname, adtree, tree_reduce, symprod, state_space, patterns, timeout=1800
):
    """
    Run a single experiment.

    Parameters
    ----------
    fname : string
        Path to the file containing the parameters of the experiment
    adtree : bool
        Flag indicating if the input is an ADTree instead of a modgraph folder
    tree_reduce : bool
        Flag indicating if the tool tree_reduce will be run
    symprod : bool
        Flag indicating if the tool symprod will be run
    state_space : bool
        Flag indicating if the state space will be computed
    patterns : bool
        Flag indicating if the patterns reduction will be run
    timeout : int
        Timeout of the experiment
    """

    modgraph_folder = fname
    if adtree:
        # translate adtree into EAMAS in modgraph format
        transformer_command(fname, "--full-model --symprod-compatibility")
        modgraph_folder = get_modgraph_folder(fname)

    # get modgraph files
    print(f"loading modgraph files from {modgraph_folder}")
    modgraph_files = get_modgraph_files(modgraph_folder)

    # run tree_reduce experiments
    if tree_reduce:
        print(f"\n {'@'*20} running tree_reduce {'@'*20}")
        run_tree_reduce(modgraph_files, timeout=timeout)

    # run symprod experiments
    if symprod:
        print(f"\n {'@'*20} running symprod {'@'*20}")
        run_prod(modgraph_files, timeout=timeout)

    # get the state space
    if state_space:
        print(f"\n {'@'*20} running statespace checker {'@'*20}")
        run_prod(modgraph_files, tool=STATE_SPACE_CHECKER, timeout=timeout)

    if adtree and patterns:
        # use the layered patterns in the squeezing paper
        print(f"\n {'@'*20} running patterns reduction {'@'*20}")
        transformer_command(fname, "--layered-reductions")
        fname_reduction = get_modgraph_folder(fname, "_patterns")
        modgraph_files_reduction = get_modgraph_files(fname_reduction)
        run_prod(modgraph_files_reduction, tool=STATE_SPACE_CHECKER, timeout=timeout)

        print(f"\n {'@'*20} running tree_reduce on patterns {'@'*20}")
        run_tree_reduce(modgraph_files_reduction, timeout=timeout)


@click.command()
@click.argument("case-study")
@click.argument("timeout", type=int)
@click.option(
    "--parameters",
    default="parameters.txt",
    help="name of the file with the parameters of the experiments",
)
@click.option(
    "--adtree/--no-adtree",
    default=True,
    help="Handle adtree or modgraphs modules as input",
)
@click.option(
    "--tree-reduce/--no-tree-reduce", default=True, help="Run tree_reduce experiments"
)
@click.option("--symprod/--no-symprod", default=True, help="Run symprod experiments")
@click.option("--patterns/--no-patterns", default=True, help="Run patterns experiments")
@click.option(
    "--state-space/--no-state-space", default=True, help="Run state space checker"
)
def main(
    case_study, timeout, parameters, adtree, tree_reduce, symprod, patterns, state_space
):
    """
    This script runs the experiments in the CASE_STUDY folder with a specific
    TIMEOUT (sec.) for each single experiment.
    """
    params = os.path.join(case_study, parameters)
    with open(params, "r") as param_file:
        params = [a for a in param_file.readlines() if len(a.strip()) > 0]
        for ctr, param in enumerate(params):
            print(f"NOW RUNNING CASE ({ctr+1})")
            param_string = param.rstrip().split()
            nb_parameters = len(param_string)

            # it's a path to an adtree model file
            if nb_parameters == 1:
                fname = os.path.join(case_study, param_string[0])
            elif nb_parameters == 3:
                width = int(param_string[0])
                depth = int(param_string[1])
                children = int(param_string[2])

                model = f"case_w{width}d{depth}c{children}.txt"
                fname = os.path.join(case_study, "models", model)

                # generate random ADTree model
                os.system(f"rm -rf {fname}")
                print("@" * 20 + " generating modgraph files " + "@" * 20)
                generate_command(width, depth, children, model)
                os.system(f"mv {model} {fname}")
            else:
                print(f"Error: parameter '{param.rstrip()}' is not supported\n")
                continue

            run_single_experiment(
                fname, adtree, tree_reduce, symprod, state_space, patterns, timeout
            )


if __name__ == "__main__":
    main()
