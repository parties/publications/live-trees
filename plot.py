#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import os
import functools
import pandas as pd
import dataframe_image as dfi


# In[ ]:


def symprod_win(row):
    nb_columns = len(row)
    symprod = row["symprod_states"]
    tree_reduction = row["reduce-tree_states"]
    
    if (symprod == -1):
        return ['background-color: #f28f88']*nb_columns      
    elif ((tree_reduction == -1) or (symprod < tree_reduction)):
        return ['background-color: #ffff8f']*nb_columns
    else:
        return ['']*nb_columns


# In[ ]:


def merge_dfs(df1, df2):
    columns = ['name', 'adtree_width', 'adtree_depth', 'adtree_type', 'adtree_children', 'adtree_leaves', 'adtree_nodes']
    return df1.merge(df2, on=columns)


# In[ ]:


def get_size_percentage(df, col1, col2):
    num = df[col1 + "_states"] + df[col1 + "_transitions"]
    den = df[col2 + "_states"] + df[col2 + "_transitions"]
    return (num/den * 100).round(3).astype(str) + " %"


# In[ ]:


def highlight_cell(df, value, color="yellow"):
    return df.style.applymap(lambda x: '' if x!=value and x != '' else f'background-color:{color}')


# In[ ]:


def convert_to_pandas(folder, random_adtree=False, symprod=False, pattern=False):
    tools = ['reduce-tree','symprod', 'state-space', 'pattern', 'pattern-layer', 'pattern-sos']
    dfs = [pd.read_csv(os.path.join(folder, 'results', f'{f}.csv')) for f in tools]
    df = functools.reduce(merge_dfs, dfs)

    # create name column
    if (random_adtree):
        df["name"] = df.agg('({0[adtree_children]}, {0[adtree_nodes]}, {0[adtree_depth]}, {0[adtree_width]})'.format, axis=1)
    
    # change order of the columns to fit the paper
    df = df[["name", "adtree_children", "adtree_nodes", "adtree_depth", "adtree_width", 
             "state-space_states", "state-space_transitions",
             "reduce-tree_states", "reduce-tree_transitions",
             "symprod_states", "symprod_transitions",
             "pattern_states", "pattern_transitions",
             "pattern-layer_states", "pattern-layer_transitions",
             "pattern-sos_states", "pattern-sos_transitions",]]
        
    # sort values
    df = df.sort_values(by=['adtree_children','adtree_depth', 'adtree_width'])
    
    # remove columns where all the values are the same
    df = df.drop(['adtree_children','adtree_depth', 'adtree_width', 'adtree_nodes'], axis=1)

    # reset index
    df = df.reset_index(drop=True)

    # compute % size
    df["reduce-tree/state_space (%)"] = get_size_percentage(df, 'reduce-tree', 'state-space')

    if (not pattern):
        df = df.drop(["pattern_states", "pattern_transitions"], axis=1)
    else:
        df["pattern/state_space (%)"] = get_size_percentage(df, 'pattern', 'state-space')

    if (not symprod):
        df = df.drop(["symprod_states", "symprod_transitions"], axis=1)
    else:
        df["symprod/state_space (%)"] = get_size_percentage(df, 'symprod', 'state-space')

    df["pattern-layer/state_space (%)"] = get_size_percentage(df, 'pattern-layer', 'state-space')
    df["pattern-sos/state_space (%)"] = get_size_percentage(df, 'pattern-sos', 'state-space')

    # replace NAN by TO
    df = df.fillna("TO")
    df = df.replace('nan %', value='')

    # convert float into int
    df = df.applymap(lambda x: int(x) if type(x) is float else x)
    df = df.applymap(lambda x: '{:,.0f}'.format(x) if type(x) is int else x)
    
    # highlight TO cells 
    highlight_table = highlight_cell(df, "TO")

    return df, highlight_table#, table, interactive_table


# In[ ]:


def generate_files(df, case_study, filename, color="red!50"):
    dest_path = os.path.join(case_study, 'tables', filename)

    func = lambda s: f"\cellcolor{{{color}}}{{{s}}}" if s == "TO" or s == '' else s
    s = df.style.format_index("\\textbf{{{}}}", escape="latex", axis=1).hide(axis='index').format(func, escape="latex")
    s.to_latex(f"{dest_path}.tex", hrules=True)

    dfi.export(df, f'{dest_path}.png', table_conversion="matplotlib")


# # Use Cases from ICFEM Paper

# In[ ]:


icfem_folder = "./case_studies/ICFEM"
df_icfem, df_icfem_h = convert_to_pandas(icfem_folder, False, False, True)
generate_files(df_icfem, icfem_folder, "table_icfem")
df_icfem_h


# # Use Cases from ICECCS Paper

# In[ ]:


iceccs_folder = "./case_studies/ICECCS"
df_iceccs, df_iceccs_h = convert_to_pandas(iceccs_folder, True, False, True)
generate_files(df_iceccs, iceccs_folder, "table_iceccs")
df_iceccs_h

