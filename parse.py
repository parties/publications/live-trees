#!/usr/bin/env python3

import csv
import math
import os
import re

import click


def parse_values(values):
    """ "Parse values from logs to their corresponding Python types"""
    new_values = []
    for value in values:
        if value:
            new_values.append(int(value) if value.isnumeric() else value)
        else:
            new_values.append(None)
    return new_values


def parse_results(filename):
    """ "Parse log file to a dictionary:
    "experiment": {"name": str},
    "adtree": {"width": int, "depth": int, "type": str, "children": int, "leaves": int, "nodes": int},
    "reduce-tree": {"states": int, "transitions": int},
    "symprod": {"states": int, "transitions": int},
    "state-space": {"states": int, "transitions": int}
    """
    case_regex = r".+CASE \((\d+)\)"
    modgraph_regex = r".+ (\S+)\.modgraph"
    adtree_regex = r".+width (\d+).+depth (\d+).+type (\w+).+(\d+) children.+file (\w+)"
    # automata_size_regex = r"\D+(\d+) automata"
    reduce_tree_regex = r"\D+(\d+) states .+ (\d+) transitions"
    symprod_regex = r"\D+(\d+) (?:nodes|states) .+ (\d+) arcs"
    state_space_regex = r"\D+(\d+).0 state\(s\) .+ (\d+).0 transition\(s\)"

    results = {}
    with open(filename, "r") as f:
        is_pattern = False
        for line in f:
            # get the number of the case
            if "RUNNING CASE" in line:
                match = re.match(case_regex, line)
                (case,) = parse_values(match.groups())
                results[case] = {
                    "experiment": {"name": None},
                    "adtree": {
                        "width": None,
                        "depth": None,
                        "type": None,
                        "children": None,
                        "leaves": None,
                        "nodes": None,
                    },
                    "reduce-tree": {"states": None, "transitions": None},
                    "symprod": {"states": None, "transitions": None},
                    "state-space": {"states": None, "transitions": None},
                    "pattern": {"states": None, "transitions": None},
                    "pattern-sos": {"states": None, "transitions": None},
                }
                continue

            # get modgraph file
            if "Modgraph model" in line:
                match = re.match(modgraph_regex, line)
                (output_filename,) = parse_values(match.groups())
                results[case]["experiment"]["name"] = output_filename
                continue

            # get information of the ADTree
            if "Generating ADT" in line:
                match = re.match(adtree_regex, line)
                width, depth, type_, children, _ = parse_values(match.groups())
                results[case]["adtree"] = {
                    "width": width,
                    "depth": depth,
                    "type": type_,
                    "children": children,
                }
                continue

            # update the depth of the ADTree
            if "depth: " in line:
                match = re.match(r"depth: (\d+)", line)
                (depth,) = parse_values(match.groups())
                results[case]["adtree"]["depth"] = depth
                continue

            # update the width of the ADTree
            if "width: " in line:
                match = re.match(r"width: (\d+)", line)
                (width,) = parse_values(match.groups())
                results[case]["adtree"]["width"] = width
                continue

            # update the leaves of the ADTree
            if "# leaves: " in line:
                match = re.match(r"# leaves: (\d+)", line)
                (leaves,) = parse_values(match.groups())
                results[case]["adtree"]["leaves"] = leaves
                continue

            # update the number of nodes of the ADTree
            if "# nodes: " in line:
                match = re.match(r"# nodes: (\d+)", line)
                (nodes,) = parse_values(match.groups())
                results[case]["adtree"]["nodes"] = nodes
                continue

            # get information of automata size
            # if "Read network" in line:
            #     match = re.match(automata_size_regex, line)
            #     automata_size, = parse_values(match.groups())
            #     results[case]["experiment"]["automata-size"] = automata_size
            #     continue

            if (
                "@ running statespace checker @" in line
                or "@ running tree_reduce @" in line
            ):
                is_pattern = False
                continue

            if (
                "@ running patterns reduction @" in line
                or "@ running tree_reduce on patterns @" in line
            ):
                is_pattern = True
                continue

            # get information of automata transitions and states
            if "EF-reducing" in line:
                match = re.match(reduce_tree_regex, line)
                reduce_tree_states, reduce_tree_transitions = parse_values(
                    match.groups()
                )

                label = "pattern-sos" if is_pattern else "reduce-tree"

                results[case][label] = {
                    "states": reduce_tree_states,
                    "transitions": reduce_tree_transitions,
                }
                continue

            # get symprod synchronization graph
            if "SYNCHRONIZATION GRAPH" in line:
                line = f.readline()  # go to the next line
                match = re.match(symprod_regex, line)
                symprod_sync_states, symprod_sync_transitions = parse_values(
                    match.groups()
                )
                results[case]["symprod"] = {
                    "states": symprod_sync_states,
                    "transitions": symprod_sync_transitions,
                }
                continue

            # get state space information
            if "Reached" in line:
                match = re.match(state_space_regex, line)
                state_space_states, state_space_transitions = parse_values(
                    match.groups()
                )

                label = "pattern" if is_pattern else "state-space"

                results[case][label] = {
                    "states": state_space_states,
                    "transitions": state_space_transitions,
                }
                continue

    return results


def write_csv(results, output_folder, tool_name, verbose=False):
    """Write the ``results`` of a tool in a csv"""
    states_field = f"{tool_name}_states"
    transitions_field = f"{tool_name}_transitions"
    fieldnames = [
        "name",
        "adtree_width",
        "adtree_depth",
        "adtree_type",
        "adtree_children",
        "adtree_leaves",
        "adtree_nodes",
        states_field,
        transitions_field,
    ]
    filename = os.path.join(output_folder, f"{tool_name}.csv")

    with open(filename, "w") as f:
        writer = csv.writer(f)
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()

        for _, result in results.items():
            if verbose:
                print(f"processing {result}")

            adtree = result["adtree"]
            tool_results = result[tool_name]
            experiment = result["experiment"]
            writer.writerow(
                {
                    "name": experiment["name"],
                    "adtree_width": adtree["width"],
                    "adtree_depth": adtree["depth"],
                    "adtree_type": adtree["type"],
                    "adtree_children": adtree["children"],
                    "adtree_leaves": adtree["leaves"],
                    "adtree_nodes": adtree["nodes"],
                    states_field: tool_results["states"],
                    transitions_field: tool_results["transitions"],
                }
            )

    print(f"see output: {filename}")


@click.command()
@click.argument("case-study")
@click.option(
    "--log",
    default="output.txt",
    help="name of the file with the logs of the experiments",
)
def main(case_study, log):
    """
    This script parses the logs of the experiments for a specific CASE_STUDY.
    """
    tools = ["reduce-tree", "state-space", "symprod", "pattern", "pattern-sos"]
    output_folder = os.path.join(case_study, "results")

    results = parse_results(os.path.join(case_study, "logs", log))
    for tool in tools:
        write_csv(results, output_folder, tool)


if __name__ == "__main__":
    main()
