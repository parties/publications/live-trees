# Modular Analysis of Tree-Topology Models

This repository hosts the results for the paper.

## Clone this repository:

```
git clone https://depot.lipn.univ-paris13.fr/parties/publications/live-trees.git && cd live-trees
```

## Folder Structure

```
├── case_studies # folder with the ADTree models and the output of the tools for each model.
└── tools        # folder with the tools needed to run the experiments
```

## Dependencies

```
pip install -r requirements.txt
```

## Run experiments

To run the experiments you need to execute:

```
./experiment.py CASE_STUDY TIMEOUT
```

where `TIMEOUT` is the timeout for each experiment, and `CASE_STUDY` is the path
to the folder containing the case study. For instance:

```
./experiment.py case_studies/ICECCS 1800 > case_studies/ICECCS/logs/output.txt
```

will store the results in the file `case_studies/ICECCS/logs/output.txt`.

## Results

Once the experiments have finished, you can parse the log file using the
script `parse.py`, as follows:

```
./parse.py --log output.txt case_studies/ICECCS
```

Note that the log file of the experiment must be in the folder `logs` of the case
study. For instance, `case_studies/ICECCS/logs/output.txt`

Finally, you can open the notebook with `jupyter notebook plot.ipynb` or
generate the tables running the script `plot.py`.

### Case studies from [1]

![ICFEM results](./case_studies/ICFEM/tables/table_icfem.png)

### Case studies from [2]

![ICECCS results](./case_studies/ICECCS/tables/table_iceccs.png)

# Authors

- Jaime Arias (LIPN, CNRS UMR 7030, Université Sorbonne Paris Nord)
- Michał Knapik (Institute of Computer Science, PAS, Warsaw University of Technology)
- Laure Petrucci (LIPN, CNRS UMR 7030, Université Sorbonne Paris Nord)
- Wojciech Penczek (Institute of Computer Science, PAS, Warsaw University of Technology)

# Abstract

Networks of automata that synchronise over shared actions are organised
according to a graph synchronisation topology. In this topology two automata are
connected if they can jointly execute some action. We present a very effective
reduction for networks with tree-like synchronisation topologies such that all
automata after synchronizing with their parents can execute only local
(non-synchronizing) actions: forever or until _resetting_, i.e. entering the
initial state. We show that the reduction preserves reachability, but not
safety. This construction is extended to tree-like topologies of arbitrary
automata and investigated experimentally.

# References

[1] Arias, Jaime, Carlos E. Budde, Wojciech Penczek, Laure Petrucci, Teofil Sidoruk, and Mariëlle Stoelinga. “Hackers vs. Security: Attack-Defence Trees as Asynchronous Multi-Agent Systems.” In 22nd International Conference on Formal Engineering Methods, ICFEM 2020, edited by Shang-Wei Lin, Zhe Hou, and Brendan P. Mahony, 12531:3–19. Lecture Notes in Computer Science. Singapore, Singapore: Springer, 2021. https://doi.org/10.1007/978-3-030-63406-3_1.

[2] Petrucci, Laure, Michal Knapik, Wojciech Penczek, and Teofil Sidoruk. “Squeezing State Spaces of (Attack-Defence) Trees.” In 2019 24th International Conference on Engineering of Complex Computer Systems (ICECCS), 71–80. Guangzhou, China: IEEE, 2019. https://doi.org/10.1109/ICECCS.2019.00015.
